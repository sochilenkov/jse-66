package ru.t1.sochilenkov.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.sochilenkov.tm.model.TaskDTO;

@Repository
public interface ITaskRepository extends JpaRepository<TaskDTO, String> {
}
