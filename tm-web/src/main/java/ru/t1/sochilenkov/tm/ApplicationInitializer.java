package ru.t1.sochilenkov.tm;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
import ru.t1.sochilenkov.tm.config.ApplicationConfiguration;
import ru.t1.sochilenkov.tm.config.WebApplicationConfiguration;

public class ApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    @NotNull
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{ApplicationConfiguration.class};
    }

    @Override
    @NotNull
    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{WebApplicationConfiguration.class};
    }

    @Override
    @NotNull
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }

}
