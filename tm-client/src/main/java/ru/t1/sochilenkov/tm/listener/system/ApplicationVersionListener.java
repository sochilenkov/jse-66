package ru.t1.sochilenkov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.sochilenkov.tm.dto.request.ApplicationVersionRequest;
import ru.t1.sochilenkov.tm.dto.response.ApplicationVersionResponse;
import ru.t1.sochilenkov.tm.event.ConsoleEvent;

@Component
public final class ApplicationVersionListener extends AbstractSystemListener {

    @NotNull
    public static final String ARGUMENT = "-v";

    @NotNull
    public static final String DESCRIPTION = "Display version of the program.";

    @NotNull
    public static final String NAME = "version";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@applicationVersionListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[VERSION]");

        @NotNull final ApplicationVersionRequest request = new ApplicationVersionRequest();

        ApplicationVersionResponse response = systemEndpoint.getVersion(request);
        System.out.println(response.getVersion());
    }

}
