package ru.t1.sochilenkov.tm.repository.model;

import org.springframework.stereotype.Repository;
import ru.t1.sochilenkov.tm.model.Session;

@Repository
public interface ISessionRepository extends IUserOwnedRepository<Session> {
}
